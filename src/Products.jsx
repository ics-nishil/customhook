import React from 'react'
import useFetch from './useFetch';

const Products = () => {
    const {data} = useFetch("https://dummyjson.com/products/")
console.log(data)
    return (
      <div className="App">
  
            <hr/>ALL PRODUCTS<hr/>
            {data && data?.products?.map(e => 
                <div>
                    <li>{e.id}</li>
                    <li>{e.title}</li>
                    <li>{e.brand}</li>
                    <li>{e.price}</li>
                    <hr/>
                </div>
                
                )
            }
        
        </div>
    );
}

export default Products