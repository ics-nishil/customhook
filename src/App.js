import './App.css';
import Item from './Item';
import Products from './Products';
import useFetch from './useFetch';

function App() {

  return (
    <div className="App">

          <Item/>      

          <Products/>
          
      </div>
  );
}

export default App;
