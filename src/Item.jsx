import React from 'react'
import useFetch from './useFetch';

const Item = () => {
    const {data} = useFetch("https://dummyjson.com/products/1")

    return (
        <div className="App">
            <hr/>Specific Product<hr/>
            
            {data && <div>
              <li>{data.id}</li>
              <li>{data.title}</li>
              <li>{data.brand}</li>
              <li>{data.price}</li>
            </div>
            }
            
        </div>
    );
}

export default Item